/*
 * Classic interface class.
 * We use to call them pure virtual classes.
 * Any class that inherits from this class
 * MUST implement all of its '= 0' methods.
 */

#ifndef _IFACE_
#define _IFACE_

namespace cicdx {

#include <string>

class Iface {
   public:
    virtual ~Iface() {}
    virtual std::string getName() = 0;
    virtual void setName(std::string&) = 0;
};

}  // namespace cicdx

#endif
