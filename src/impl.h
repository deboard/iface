#ifndef _IMPL_
#define _IMPL_

#include <string>
#include "iface.h"

namespace cicdx {

// class that implements the Iface interface
class Impl : public Iface {
    std::string name;

   public:
    Impl() : name("nill") {}
    virtual ~Impl() {}
    virtual std::string getName() override { return name; }
    virtual void setName(std::string& vname) override { name = vname; }
};

}  // namespace cicdx

#endif
