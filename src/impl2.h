#ifndef _IMPL2_
#define _IMPL2_

#include <string>
#include "iface.h"

namespace cicdx {

// class that implements the Iface interface
class Impl2 : public Iface {
    std::string name;

   public:
    Impl2() : name("nill") {}
    virtual ~Impl2() {}
    virtual std::string getName() override { return name; }
    virtual void setName(std::string& vname) override { name = vname; }
};

}  // namespace cicdx

#endif
