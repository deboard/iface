#include <iostream>
#include "iface.h"
#include "impl.h"
#include "impl2.h"

using namespace std;
using namespace cicdx;

/*
 * Program that demonstrates the use of interfaces
 * and how to impliment them.
 *
 * Mainly the program is to test a Gitlab CI/CD
 * pipeline.
 * */

/*

// some code for cppcheck to look at
void crashMe(int z) {
    int p = 2 / z;
    cout << p << endl;
    int* o = nullptr;
    int q = *o / z;
    cout << q << endl;
}

*/

int main() {
    cout << "IFACE" << endl;

    Iface* face = nullptr;
    Impl* imp = new Impl();
    Impl2* imp2 = new Impl2();

    Impl2 aimp;

    face = imp;
    std::string pname = "foo";
    face->setName(pname);
    cout << face->getName() << endl;

    face = imp2;
    pname = "bar";
    face->setName(pname);
    cout << face->getName() << endl;

    face = &aimp;
    pname = "woot";
    face->setName(pname);
    cout << face->getName() << endl;

    delete imp;
    delete imp2;
}
