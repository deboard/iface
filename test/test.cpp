#include <gtest/gtest.h>
#include <string>
#include "iface.h"
#include "impl.h"
#include "impl2.h"

namespace {

using namespace cicdx;

class MyTest : public testing::Test {
    protected:
    void SetUp() override {}
    void TearDown() override {}

};

TEST_F(MyTest, Test1) {
    std::string t1 = "nill";
    Iface* iface = nullptr;
    Impl aimpl;
    iface = &aimpl;
    EXPECT_EQ(t1, iface->getName());
}
  
TEST_F(MyTest, Test2) {
    std::string t1 = "Fred";
    Iface* iface = nullptr;
    Impl aimpl;
    iface = &aimpl;
    iface->setName(t1);
    EXPECT_EQ(t1, iface->getName());
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

}
